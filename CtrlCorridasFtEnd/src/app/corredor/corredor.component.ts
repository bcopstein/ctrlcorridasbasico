import { Component, OnInit } from '@angular/core';
import { Corredor } from '../corredor';
import { CorredorService } from '../corredor.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-corredor',
  templateUrl: './corredor.component.html',
  styleUrls: ['./corredor.component.css']
})
export class CorredorComponent implements OnInit {
  corredor:Corredor;
  corredor2:Corredor;

  constructor(
    private corredorService:CorredorService,
    private messageService:MessageService) {
      this.corredor = {
        cpf:"001",
        nome:"",
        diaDn:0,
        mesDn:0,
        anoDn:0,
        genero:"",
        eventos:[]
      }
     }

  ngOnInit(): void {
    this.corredorService.carregar().subscribe(umCorredor => this.corredor = umCorredor);
  }

  get msgService():MessageService{
    return this.messageService;
  }

  load(){
    this.corredorService.carregar().subscribe(umCorredor => this.corredor = umCorredor);
  }

  save(){}
}
