import { Evento } from './evento';

export interface Corredor {
  cpf: String,
  nome: String,
  diaDn: number,
  mesDn: number,
  anoDn: number,
  genero: String
  eventos:Evento[];
}

