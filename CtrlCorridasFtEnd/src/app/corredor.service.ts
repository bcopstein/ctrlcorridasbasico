import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Corredor } from './corredor';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root',
})
export class CorredorService {

  private serverCorredorUrl = 'http://localhost:8080/ctrlCorridas/corredor';
  private corredor:Observable<Corredor>;

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
      this.corredor = new Observable<Corredor>();
    }

  carregar():Observable<Corredor>{
    return this.http.get<Corredor>(this.serverCorredorUrl);
  }

  salvarDadosCorredor(corredor:Corredor){
    // TODO: enviar os dados para o servidor
  }

  private log(message: string) {
    this.messageService.add(`CorredorService: ${message}`);
  }

}
