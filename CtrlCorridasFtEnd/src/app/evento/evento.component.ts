import { Component, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

import { Evento } from '../evento';
import { EventoService } from '../evento.service';
import { CorredorService } from '../corredor.service';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {
  evento:Evento;
  @Output() novoEvento = new EventEmitter<void>();

  constructor(private eventoService:EventoService) {
    this.evento = {
      id:0,
      nome:"teste",
      dia:0,
      mes:0,
      ano:0,
      distancia:0,
      horas:0,
      minutos:0,
      segundos:0
    }
   }

  ngOnInit(): void {
  }

  save(){
    this.eventoService
    .addEvent(this.evento)
    .subscribe(flag => {
      if (flag){
        this.novoEvento.emit();
      }
    });
  }
}
