import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import {Evento} from './evento';

@Injectable({
  providedIn: 'root'
})
export class EventoService {
  private serverEventoUrl = 'http://localhost:8080/ctrlCorridas/evento';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  addEvent(evento: Evento): Observable<Evento> {
    return this.http.post<Evento>(this.serverEventoUrl, evento,this.httpOptions);
  }
}

