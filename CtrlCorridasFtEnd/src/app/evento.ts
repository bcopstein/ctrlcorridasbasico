export interface Evento{
  id:Number;
  nome:String;
  // Data do evento
  dia:Number;
  mes:Number;
  ano:Number;
  // Distancia percorrida
  distancia:Number; // metros
  // Tempo que o corredor levou para percorrer a distancia
  horas:Number;
  minutos:Number;
  segundos:Number;
}
