package com.bcopstein;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Corredor {
    @Id
    private String cpf;
    private String nome;
    private int diaDn,mesDn,anoDn;
    private String genero;
    @OneToMany(cascade = CascadeType.ALL) // O Cascadeype é fundamental para que ele salve tudo junto
    //@JoinColumn(name = "corredor_id") // Esse é apenas para relacionamentos bidirecionais. Seria a coluna que mapeia de volta
    private List<Evento> eventos;

    public Corredor(String cpf, String nome, int diaDn,int mesDn,int anoDn, String genero) {
        this.cpf = cpf;
        this.nome = nome;
        this.diaDn = diaDn;
        this.mesDn = mesDn;
        this.anoDn = anoDn;
        this.genero = genero;
        eventos = new LinkedList<>();
    }

    protected Corredor(){

    }

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public int getDiaDn() {
        return diaDn;
    }

    public int getMesDn() {
        return mesDn;
    }
    public int getAnoDn() {
        return anoDn;
    }

    public String getGenero() {
        return genero;
    }

    public List<Evento> getEventos(){
        return eventos;
    }

    public void informaEvento(Evento e){
        eventos.add(e);
    }

    @Override
    public String toString() {
        return "Corredor [anoDn=" + anoDn + ", cpf=" + cpf + ", diaDn=" + diaDn + ", eventos=" + eventos + ", genero="
                + genero + ", mesDn=" + mesDn + ", nome=" + nome + "]";
    }
}