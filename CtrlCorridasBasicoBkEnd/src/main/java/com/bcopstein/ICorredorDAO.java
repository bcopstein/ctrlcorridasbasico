package com.bcopstein;

import org.springframework.data.repository.CrudRepository;

public interface ICorredorDAO extends CrudRepository<Corredor, String> {
    
}
