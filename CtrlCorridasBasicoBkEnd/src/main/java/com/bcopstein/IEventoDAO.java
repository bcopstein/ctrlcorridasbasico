package com.bcopstein;

import org.springframework.data.repository.CrudRepository;

public interface IEventoDAO extends CrudRepository<Evento, String> {
    
}


