package com.bcopstein;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ctrlCorridas")
public class CtrlPessoalCorridasControler {
  private ICorredorDAO persisteCorredor;
  private IEventoDAO persisteEvento;

  @Autowired
  public CtrlPessoalCorridasControler(ICorredorDAO persisteCorredor, IEventoDAO persisteEvento) {
    this.persisteCorredor = persisteCorredor;
    this.persisteEvento = persisteEvento;
  }

  @GetMapping("/corredor")
  @CrossOrigin(origins = "*")
  public Corredor consultaCorredor() {
    List<Corredor> corredor = (List<Corredor>) persisteCorredor.findAll();
    if (corredor.size() == 0){
      //return(new Corredor("000000000","none",01,01,1900,"nao declarado"));
      return null;
    }else{
      return corredor.get(0);
    }
  }

  
  @PostMapping("/corredor")
  @CrossOrigin(origins = "*")
  public boolean cadastraCorredor(@RequestBody final Corredor corredor) {
    // Limpa a base de dados
    persisteCorredor.deleteAll();
    // Então cadastra o novo "corredor único"
    persisteCorredor.save(corredor);
    return true;
  }

  @PostMapping("/evento") // adiciona evento no único corredor
  @CrossOrigin(origins = "*")
  public boolean informaEvento(@RequestBody final Evento evento) {
    // Persiste o evento
    persisteEvento.save(evento);
    // Recupera o corredor
    Corredor corredor = consultaCorredor(); // Note que está chamando um método que atende um endpoint
    // Acrescenta o evento no corredor
    corredor.informaEvento(evento);
    // Persiste o corredor
    persisteCorredor.save(corredor);
    return true;
  }
}
