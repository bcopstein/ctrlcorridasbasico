### 46529-06 Projeto e Arquitetura de Software
# Ambiente de Desenvolvimento

Conjunto de arquivos de configuração para executar o **Visual Studio Code** dentro de um contêiner Docker. Baseado em Debian 10 (Buster), o ambiente contém as ferramentas que serão utilizadas na disciplina de Projeto e Arquitetura de Software do curso de Engenharia de Software da PUCRS.

## Instalação

1. Copie este repositório para a sua máquina local usando o comando `git clone <URL do Repositório>`
2. Abra o **Visual Studio Code**
3. Navegue até o menu "Extensões" e instale a extensão "Remove - Containers" da Microsoft (ou procure pelo id: `ms-vscode-remote`)
4. Uma vez instalada a extensão, um ícone aparecerá no extremo inferior esquerdo da janela. O ícone tem a aparência de duas setas apontando uma para a outra
5. Clique no ícone e selecione a opção "Re-abrir em Contêiner"
6. Na sua primeira execução, levará alguns minutos até que a inicialização do contêiner ocorra. Em execuções subsequentes, esse deverá ser um processo rápido

## Utilização

A janela corrente do Visual Studio Code agora estará executando de dentro do contêiner. Todos os arquivos do projeto atual estarão espelhados entre a máquina atual e o contêiner, ou seja, qualquer modificação vinda da máquina local impactará o arquivo no contêiner e vice-versa.

Para executar comandos dentro do contêiner, utilize o terminal integrado do **Visual Studio Code**. Ele estará automaticamente configurado para executar no ambiente remoto.

## Atribuição

Este projeto foi desenvolvido por [Rafael Copstein](https://github.com/rcopstein) para a disciplina de Projeto e Arquitetura de Software do curso de Engenharia de Software da PUCRS ministrada pelo professor Bernardo Copstein.